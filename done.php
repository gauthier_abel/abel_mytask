<?php
	require_once('inc/config.php');

	print_r($_REQUEST);

	if ($_REQUEST['status'] == "open") {
		$query = $db -> prepare('UPDATE task SET done_by = ?, status = "done" WHERE id = ?');
		$query -> execute(array($_SESSION['userid'], $_GET['id']));
	}
	else {
		$query = $db -> prepare('UPDATE task SET done_by = ?, status = "open" WHERE id = ?');
		$query -> execute(array($_SESSION['userid'], $_GET['id']));
	}

	header('location:index.php');
?>