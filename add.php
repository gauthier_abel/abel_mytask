<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('templates/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('templates/header-add.php'); ?>

			<main>
				<div class="add-form">
					<form method="post" action="update.php">
			            <label>Description</label>
			            <textarea name="description" rows="6"></textarea>
			            <label>Priority</label>
			            <select name="priority">
			              <?php for($i = 1; $i <= 5; $i++): ?>
			                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			              <?php endfor; ?>
			            </select>
			            <label>Due date</label>
			            <input type="date" name="due_at"/>
						<label>Assigned to</label>
						<select name="assigned_to">
							<?php $query = $db -> query('SELECT * FROM user'); while($data =	$query -> fetch()): ?>
								<option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
							<?php endwhile; ?>
		            	</select>
			            <input type="submit" value="Ajouter" class="button"/>
			        </form>
				</div>
			</main>
		</div>

	    <script src="bower_components/jquery/dist/jquery.js"></script>
	    <script src="bower_components/what-input/dist/what-input.js"></script>
	    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	    <script src="js/app.js"></script>
	</body>
</html>