<?php
require_once('inc/config.php');

// if(isset($_SESSION['userid']))
// 	header('Location:index.php');

$query = $db -> prepare('SELECT id FROM user WHERE email = ? AND password = ?');
$query -> execute(array($_POST['email'], $_POST['password']));
$result = $query -> fetch();

if($result != null) {
	$_SESSION['userid'] = $result['id'];
	header('Location:index.php');
}

else
	header('Location:login.php');
?>
