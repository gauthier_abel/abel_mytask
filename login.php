<?php
require_once('inc/config.php');

//if(isset($_SESSION['userid']))
//	header('Location:index.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Task List Login</title>
	    <meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>
	<main class="login-main">
		<h1>Login</h1>
		<form method="post" action="logme.php">
			<label for="email" style="color: white;">E-mail</label>
			<input type="text" name="email" id="email" style="border-radius: 10px;" />
			<label for="password" style="color: white;">Mot de passe</label>
			<input type="password" name="password" id="password" style="border-radius: 10px;" />

			<input type="submit" value="Se connecter" class="button" style="background-color: #338595; border-radius: 10px;" />
		</form>
	</main>

	<?php require_once('inc/script.php'); ?>
  </body>
</html>
