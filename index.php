<?php require_once 'inc/config.php';?>
<?php require_once 'inc/security.php';?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php require_once('templates/head.php'); ?>
  </head>
  <body>
    <?php require_once('templates/header.php'); ?>
    <main>
      <div class="toolbar">
        <span>
          Sort by : 
          <select class="sort-select">
            <option value="ID">ID</option>
            <option value="Author">Author</option>
            <option value="Assignee">Assignee</option>
            <option value="Creation_date">Creation date</option>
            <option value="Due_date">Due date</option>
          </select>
        </span>
        <a href="sort.php?by=due_at" class="button sort-button">Apply</a>
      </div>
      <div class="container">
          <ul class="tasklist">
              <li class="table-title">
                  <span class="id">
                      ID
                  </span>
                  <span class="description">
                      Description
                  </span>
                  <span class="created-at">
                      Created
                  </span>
                  <span class="due-at">
                      Due
                  </span >
                  <span class="priority">
                      Priority
                  </span>
                  <span class="status">
                      Status
                  </span>
                  <span class="delete"></span>
              </li>

              <?php foreach ($data as $row) :?>
              <li title="Edit task" onclick="location.href='edit.php?id=<?php echo $row['id']?>'";>
                  <div>
                    <span class="id">
                        <?php echo $row['id']?>
                    </span>
                    
                    <?php if ($row['status'] == "open"): ?>
                      <span class="description">
                        <?php echo $row['description']?>
                      </span>
                    <?php else: ?>
                      <span class="description label-close" >
                        <?php echo $row['description']?>
                          </span>
                    <?php endif ?>

                    <?php if ($row['status'] == "open"): ?>
                      <span class="created-at">
                        <?php echo $row['created_at']?>
                      </span>
                    <?php else: ?>
                      <span class="created-at label-close" >
                        <?php echo $row['created_at']?>
                          </span>
                    <?php endif ?>

                    <?php if ($row['status'] == "open"): ?>
                      <span class="due-at">
                        <?php echo $row['due_at']?>
                      </span>
                    <?php else: ?>
                      <span class="due-at label-close" >
                        <?php echo $row['due_at']?>
                          </span>
                    <?php endif ?>
    
                    <?php if ($row['status'] == "open"): ?>
                      <span class="priority">
                        <?php echo $row['priority']?>
                      </span>
                    <?php else: ?>
                      <span class="priority label-close" >
                        <?php echo $row['priority']?>
                      </span>
                    <?php endif ?>
                    <span class="status">
                        <?php if ($row['status'] == "open"): ?>
                          <a href="done.php?id=<?php echo $row['id'];?>&status=<?php echo $row['status']?>" class="btn-status-red btn-text" title="Mark as done">​</a>
                        <?php else: ?>
                          <a href="done.php?id=<?php echo $row['id'];?>&status=<?php echo $row['status']?>" class="btn-text btn-status-green" title="Unmark as done">
                            &#10003;
                          </a>
                        <?php endif ?>
                    </span>
                    <span class="delete">
                      <a href="delete.php?id=<?php echo $row['id'];?>" class="btn-text btn-delete" title="Delete task">
                        ❌
                      </a>
                    </span>
                    <div class="author">
                      <span class="people">Created by <?php echo $row['author']?> for <?php echo $row['assignee']?></span>
                      <?php if ($row['status'] == "done"): ?>
                        <span class="people">Done by : <?php echo $row['executer']?></span>
                      <?php else: ?>
                      <?php endif ?>
                    </div>
                  </div>
              </li>
              <?php endforeach;?>
          </ul>
      </div>
    </main>

    <?php require_once 'templates/footer.php';?>

    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>