<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>

<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('templates/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('templates/header-adduser.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="add-form">
					<form method="post" action="adduser-apply.php">
			            <label>Name</label>
			            <input type="text" name="name"/>
						<label>E-mail address</label>
			            <input type="text" name="email"/>
						<label>Password</label>
			            <input type="password" name="password"/>
			            <input type="submit" value="Ajouter" class="button"/>
			        </form>
				</div>
			</main>
		</div>
		
		<script src="bower_components/jquery/dist/jquery.js"></script>
	    <script src="bower_components/what-input/dist/what-input.js"></script>
	    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	    <script src="js/app.js"></script>
	</body>
</html>
