<header>
	<div class="top-bar">
		<div class="top-bar-title">
		  <ul class="dropdown menu" data-dropdown-menu>
		    <li class="menu-text">
		      <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
		    </li>
		    <li class="menu-text"><strong>Task list</strong></li>
		  </ul>
		</div>
		<div class="top-bar-right">
		  <img src="img/profile.png" title="swag" alt="profile picture" class="profile">
		</div>
	</div>
    <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    	<a href="logout.php" class="button sidebar-button" style="background-color: red;">Log out</a>
    	<a href="adduser.php" class="button sidebar-button">Create an user</a>
    	<a href="img/about.pdf" class="button sidebar-button">About this tool</a>
    	<a href="CV/curriculum.html" class="button sidebar-button">About the creator</a>
    </div>
</header>