<?php
	$db = new PDO('mysql:host=localhost;dbname=taskList;charset=utf8mb4', 'abelg', '12345678');

	$stmt = $db->prepare("SELECT
							task.id, 
							task.description, 
							task.created_at, 
							task.due_at,
							author.name as author,
							assignee.name as assignee,
							executer.name as executer,
							task.priority,
							task.status
						FROM task

						INNER JOIN user author ON task.created_by = author.id
						LEFT JOIN user assignee ON task.assigned_to = assignee.id
						LEFT JOIN user executer ON task.done_by = executer.id ORDER BY by");
	$stmt -> execute(array($_GET['by']));
	$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
	header('location:index.php');
;?>