<?php
require_once('inc/config.php');
require_once('inc/security.php');

if(isset($_POST['id'])) {
	$query = $db -> prepare('UPDATE task SET description = ?, priority = ?, due_at = ?, assigned_to = ? WHERE id = ?');
  	$query -> execute(array($_POST['description'], $_POST['priority'], $_POST['due_at'], $_POST['assigned_to'], $_POST['id']));
}

else {
	$query = $db -> prepare('INSERT INTO task(description, created_by, due_at, assigned_to, priority) VALUES(?, ?, ?, ?, ?)');
  	$query -> execute(array($_POST['description'], $_SESSION['userid'], $_POST['due_at'], $_POST['assigned_to'], $_POST['priority']));
}

header('Location:index.php');
?>
