-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip` int(11) NOT NULL,
  `name` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `city` (`id`, `zip`, `name`) VALUES
(1,	1754,	'Avry'),
(2,	1700,	'Fribourg'),
(3,	1783,	'Barberêche'),
(4,	1726,	'Farvagny');

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` char(255) NOT NULL,
  `created_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `due_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `done_by` int(11) NOT NULL,
  `priority` enum('1','2','3','4','5') NOT NULL,
  `status` enum('open','done') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `task` (`id`, `description`, `created_at`, `due_at`, `created_by`, `assigned_to`, `done_by`, `priority`, `status`) VALUES
(1,	'Swag',	'2017-06-26 11:23:17',	'2017-06-15 00:00:00',	4,	4,	1,	'4',	'open'),
(2,	'Lorem ipsum tout ça',	'2017-06-26 10:59:01',	'2017-06-26 00:00:00',	4,	2,	1,	'2',	'done'),
(4,	'Lorem ipsum tout ça',	'2017-06-25 23:01:56',	'2017-06-26 00:00:00',	4,	2,	1,	'2',	'open'),
(6,	'Lorem ipsum tout ça',	'2017-06-25 23:01:56',	'2017-06-26 00:00:00',	4,	2,	1,	'2',	'open'),
(7,	'Lorem ipsum tout ça',	'2017-06-26 09:30:41',	'2017-06-26 00:00:00',	4,	2,	5,	'2',	'done'),
(8,	'Lorem ipsum tout ça',	'2017-06-25 23:10:45',	'2017-06-26 00:00:00',	4,	2,	3,	'2',	'open'),
(9,	'Lorem ipsum tout ça',	'2017-06-25 23:06:42',	'2017-06-26 00:00:00',	4,	2,	5,	'2',	'done'),
(13,	'Lorem ipsum tout ça',	'2017-06-25 23:05:32',	'2017-06-26 00:00:00',	4,	2,	1,	'2',	'done'),
(15,	'Lorem ipsum tout ça',	'2017-06-25 23:10:40',	'2017-06-26 00:00:00',	4,	2,	3,	'2',	'done'),
(16,	'Lorem ipsum tout ça',	'2017-06-25 23:01:56',	'2017-06-26 00:00:00',	4,	2,	1,	'2',	'open'),
(17,	'Lorem ipsum tout ça',	'2017-06-25 23:06:09',	'2017-06-26 00:00:00',	4,	2,	5,	'2',	'done'),
(18,	'Lorem ipsum tout ça',	'2017-06-25 23:05:29',	'2017-06-26 00:00:00',	4,	2,	1,	'2',	'done'),
(19,	'Lorem ipsum tout ça',	'2017-06-25 23:01:56',	'2017-06-26 00:00:00',	4,	2,	1,	'2',	'open'),
(20,	'Lorem ipsum tout ça',	'2017-06-25 23:05:59',	'2017-06-26 00:00:00',	4,	2,	5,	'2',	'done');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(127) NOT NULL,
  `city_id` int(11) NOT NULL,
  `origin_id` int(11) NOT NULL,
  `email` char(127) NOT NULL,
  `password` char(127) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `name`, `city_id`, `origin_id`, `email`, `password`) VALUES
(1,	'Gauthier Abel',	1,	1,	'gauthierabel@gmail.com',	'password'),
(2,	'Kevin Da Silva',	2,	2,	'keke@windowslive.ch',	'JeSuisSuisse'),
(3,	'Patrick Audriaz',	2,	3,	'patrick@audriaz.ch',	'patatemasquee'),
(4,	'Mickael Reynaud',	2,	4,	'soultuer.god@gmail.fr',	'BonjourJeFaisDesPodcast'),
(5,	'abelg',	1,	1,	'abelg',	'12345678');

-- 2017-06-26 10:03:55
